import processing.serial.*;
import controlP5.*;
import java.time.LocalDateTime;
import java.time.format.*;
static final String portName = "/dev/cu.usbserial";
static final String APP_TITLE = "faart";
String buf = "";
PFont myFont;
Serial myPort;
boolean logging = false;
ShittyCSVLogger csv = null;
int rowsWritten=0;
int loggingTimer=0;

String[] fields = {
  "PeakC", 
  "PeakFS", 
  "SPLZF", 
  "SPLAF", 
  "SPLCF", 
  "SPLZS", 
  "SPLAS", 
  "SPLCS", 
  "LZeq1", 
  "LAeq1", 
  "LCeq1", 
  "LZeq10", 
  "LAeq10", 
  "LCeq10"
};

float[] values = new float [fields.length];
float[] max = new float [fields.length];


void setup() {
  size (550, 1000);
  cp5Init();
  myPort=new Serial (this, portName, 115200);
  myFont = createFont("Consolas", 32);
  textFont (myFont);
  textAlign (LEFT, TOP);
  textSize(50);
  fill(255);
  dbg ("press space to reset max\n");
}

void draw() {
  background(64);
  fill(255);
  text("PARAM  VALUE MAX", 10, 10);
  stroke(255);
  strokeWeight(2);
  textSize(20);
  line(10, 28, 205, 28);
  for (int i=0; i<fields.length; i++) {
    fill(255);
    text(String.format ("%6s  ", fields[i]), 10, 32+(20*i));
    fill(getCol(values[i]));
    text(String.format ("%12.1f", values[i]), 10, 32+(20*i));
    fill(getCol(max[i]));   
    text(String.format ("%18.1f", max[i]), 10, 32+(20*i));
  }
  
  if (millis() > loggingTimer && logging) {
    loggingTimer=millis()+10000;
    logStuff();
    surface.setTitle(String.format("%s [logging n=%d]",APP_TITLE, rowsWritten));
  }
}

color getCol(float f) {
  if (f<100) return  color (0, 255, 0);
  if (f<103) return  color (255, 255, 0);
  return color (255, 0, 0);
}

void serialEvent(Serial myPort) {
  if ( myPort.available() > 0) {
    int b = myPort.read();
    if (b=='\n')
      process();
    else {
      buf+=(char)b;
    }
  }
}

// process stuff from serial port
void process() {
  if (buf.startsWith("*")) {
    String[] s = buf.split("=");
    //not clever
    for (int i=0; i<fields.length; i++) {
      if (fields[i].equals(s[0].substring(1))) { 
        // found it
        try {
          values[i] = Float.parseFloat(s[1]);
          if (values[i] > max[i]) max[i]=values[i];
        } 
        catch (Exception e) {
          //probably just some serial corruption, carry on
        }
      }
    }
  } else {
    dbg(buf+"\n");    // we stripped \n earlier so add it back
  }
  buf="";
}

void keyPressed() {
  if (keyCode==32) {
    for (int i =0; i<max.length; i++) {
      max[i]=0;
    }
  } else if (keyCode == 93 && logging) {
    //stop logging
    csv.close();
    csv = null;
    surface.setTitle(APP_TITLE); 
    logging=false;
  } else if (keyCode == 91 && !logging) {
    csv = new ShittyCSVLogger ("/Users/owen/Desktop/log.csv");
    //first a time column
    csv.add ("Time");
    //then all the measurement columns
    for (int i=0; i<fields.length; i++) {
      csv.add (fields[i]);
    }
    csv.writeLine();
    rowsWritten=0;
    
    logging=true;
  } else if ( keyCode == 67) {        // c
    dbgClear();
  } else {
    println (keyCode);
  }
}